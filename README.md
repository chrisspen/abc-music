ABC Music
=========

[ABC](http://abcnotation.com/wiki/abc:standard:v2.1) files for various music lesson books.

Meant for use with the online music training tool [Note Scroller](https://www.notescroller.com/).
