#!/usr/bin/env python3
import os
import hashlib

def get_file_hash(fin):
    h = hashlib.sha512()
    with open(fin, errors='ignore') as fin:
        text = fin.read()
        if not isinstance(text, str):
            text = str(text, encoding='utf-8', errors='replace')
        h.update(text.encode('utf-8', 'replace'))
    return h.hexdigest()

def get_hash_fn(fn):
    root, base = os.path.split(fn)
    return os.path.join(root, '.'+base)

def get_pdf_fn(fn):
    base, ext = os.path.splitext(fn)
    return base + '.pdf'

def update_hash(fn):
    hash_fn = get_hash_fn(fn)
    file_hash = get_file_hash(fn)
    with open(hash_fn, 'w') as fout:
        fout.write(file_hash)

def needs_pdf_generated(fn):
    pdf_fqfn = get_pdf_fn(fn)
    if not os.path.exists(pdf_fqfn):
        return True
    hash_fn = get_hash_fn(fn)
    if not os.path.exists(hash_fn):
        return True
    current_file_hash = get_file_hash(fn).strip()
    with open(hash_fn) as fin:
        last_file_hash = fin.read().strip()
    return current_file_hash != last_file_hash

def check_all(limit=0, name=''):
    i = 0
    all_files = list(os.walk(".", topdown=False))
    total = 0
    for root, dirs, files in all_files:
        for fn in files:
            if (name and name not in fn) or fn.startswith('.') or not fn.endswith('.abc'):
                continue
            total += 1
    for root, dirs, files in all_files:
        for fn in files:
            if (name and name not in fn) or fn.startswith('.') or not fn.endswith('.abc'):
                continue
            i += 1
            print(f'Checking {i} of {total}: {root}/{fn}...')
            fqfn = os.path.join(root, fn)
            if needs_pdf_generated(fqfn):
                print('Regenerating pdf...')
                base_dir, local_fn = os.path.split(fqfn)
                cmd = f'cd {base_dir}; abc2pdf {local_fn}'
                print(cmd)
                os.system(cmd)
                print('Updating hash...')
                update_hash(fqfn)
            else:
                print('Fresh.')
            if limit and i > limit:
                return

check_all()
